### Confluence Karmar Test Runner Support ###

This artifact contains the custom soy function for pre-transform soy template to js.

#### Karma tests ####
Assuming that you have a karma profile like this 

        <profile>
            <!-- Profile for Karma unit testing. Skips all other tests -->
            <id>karma</id>
            <dependencies>
                <!-- Only exists for qunit -->
                <dependency>
                    <groupId>com.atlassian.aui</groupId>
                    <artifactId>auiplugin</artifactId>
                    <version>${aui.version}</version>
                    <scope>test</scope>
                </dependency>
                <dependency>
                    <groupId>com.atlassian.plugins</groupId>
                    <artifactId>jquery</artifactId>
                    <version>${jquery.version}</version>
                    <scope>test</scope>
                </dependency>
            </dependencies>
            <build>
                <plugins>
                    <plugin>
                        <!-- Install npm modules first -->
                        <groupId>org.codehaus.mojo</groupId>
                        <artifactId>exec-maven-plugin</artifactId>
                        <version>1.2.1</version>
                        <executions>
                            <execution>
                                <id>install-npm-modules</id>
                                <phase>generate-test-sources</phase>
                                <goals>
                                    <goal>exec</goal>
                                </goals>
                            </execution>
                        </executions>
                        <configuration>
                            <executable>npm</executable>
                            <arguments>
                                <argument>install</argument>
                            </arguments>
                        </configuration>
                    </plugin>
                    <plugin>
                        <!-- Extract AUI for use in tests -->
                        <artifactId>maven-dependency-plugin</artifactId>
                        <executions>
                            <execution>
                                <id>extract-jquery-dependencies</id>
                                <phase>generate-test-resources</phase>
                                <goals>
                                    <goal>unpack-dependencies</goal>
                                </goals>
                                <configuration>
                                    <outputDirectory>${project.build.directory}/jquery</outputDirectory>
                                    <includeGroupIds>com.atlassian.plugins</includeGroupIds>
                                    <includeArtifactIds>jquery</includeArtifactIds>
                                    <includes>**/*.js</includes>
                                </configuration>
                            </execution>
                            <execution>
                                <id>extract-qunit-dependencies</id>
                                <phase>generate-test-resources</phase>
                                <goals>
                                    <goal>unpack-dependencies</goal>
                                </goals>
                                <configuration>
                                    <outputDirectory>${project.build.directory}/qunit/dependencies</outputDirectory>
                                    <includeGroupIds>com.atlassian.aui</includeGroupIds>
                                    <includeArtifactIds>auiplugin</includeArtifactIds>
                                    <includes>**/*.js</includes>
                                </configuration>
                            </execution>
                        </executions>
                        <configuration>
                            <useJvmChmod>true</useJvmChmod>
                        </configuration>
                    </plugin>
                    <plugin>
                        <!-- Run the karma tests during test phase -->
                        <groupId>com.kelveden</groupId>
                        <artifactId>maven-karma-plugin</artifactId>
                        <version>1.6</version>
                        <executions>
                            <execution>
                                <id>run-karma-tests</id>
                                <goals>
                                    <goal>start</goal>
                                </goals>
                            </execution>
                        </executions>
                        <configuration>
                            <!-- Use Phantom for the automated build because I couldn't get Chrome to work on a Bamboo agent -->
                            <browsers>PhantomJS</browsers>
                            <karmaExecutable>node_modules/karma/bin/karma</karmaExecutable>
                        </configuration>
                    </plugin>

                    <plugin>
                        <groupId>com.atlassian.maven.plugins</groupId>
                        <artifactId>soy-to-js-maven-plugin</artifactId>
                        <version>1.8</version>
                        <dependencies>
                            <dependency>
                                <groupId>com.atlassian.confluence</groupId>
                                <artifactId>confluence-karmar-testrunner-support</artifactId>
                                <version>0.0.1</version>
                            </dependency>
                        </dependencies>
                        <executions>
                            <execution>
                                <id>generate-js-from-soy</id>
                                <phase>generate-test-resources</phase>
                                <goals>
                                    <goal>compile</goal>
                                </goals>
                                <configuration>
                                    <module>com.atlassian.confluence.test.support.karmar.CustomFunctionTransformModule</module>
                                    <outputDirectory>${project.build.directory}/qunit/soy</outputDirectory>
                                    <resources>
                                        <directory>${project.basedir}/src/main/resources/</directory>
                                        <includes>
                                            <include>**/*.soy</include>
                                        </includes>
                                    </resources>
                                    <propertiesFile>${project.basedir}/src/main/resources/confluence-inline-comments.properties</propertiesFile>
                                </configuration>
                            </execution>
                        </executions>
                    </plugin>

                    <!-- skip ITs -->
                    <plugin>
                        <groupId>com.atlassian.maven.plugins</groupId>
                        <artifactId>maven-confluence-plugin</artifactId>
                        <version>${amps.version}</version>
                        <configuration>
                            <skipITs>true</skipITs>
                        </configuration>
                    </plugin>
                </plugins>
            </build>
            <properties>
                <!-- Skips unit tests -->
                <maven.test.unit.skip>true</maven.test.unit.skip>
            </properties>
        </profile>
        
To run the qunit test suite with Karma from the command line: ``mvn3 test -P karma -Dcontext.path=confluence -Ddark.feature.enable=confluence-inline-comments``.
where : 
* context.path is the expected relative value returned by contextpath() function
* dark.feature.enable is the comma separated value, for each you want isDarkFeatureEnabled('darkfeaturename') to return true.