package com.atlassian.confluence.test.support.karma;

public class MockDocLinkFunction extends MockHelpUrlFunction
{
    @Override
    public String getName()
    {
        return "docLink";
    }
}
