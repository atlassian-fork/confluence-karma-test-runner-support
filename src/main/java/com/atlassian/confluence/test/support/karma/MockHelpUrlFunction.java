package com.atlassian.confluence.test.support.karma;

import java.util.List;
import java.util.Set;

import com.google.common.collect.Sets;
import com.google.template.soy.data.SoyData;
import com.google.template.soy.jssrc.restricted.JsExpr;
import com.google.template.soy.jssrc.restricted.SoyJsSrcFunction;
import com.google.template.soy.tofu.restricted.SoyTofuFunction;

public class MockHelpUrlFunction implements SoyJsSrcFunction, SoyTofuFunction
{

    @Override
    public String getName()
    {
        return "helpUrl";
    }

    @Override
    public Set<Integer> getValidArgsSizes()
    {
        final Set<Integer> ret = Sets.newHashSet();
        ret.add(1);
        return ret;
    }

    @Override
    public SoyData computeForTofu(final List<SoyData> args)
    {
        return null;
    }

    @Override
    public JsExpr computeForJsSrc(final List<JsExpr> args)
    {
        return new JsExpr(args.get(0).getText(), 0);
    }

}
